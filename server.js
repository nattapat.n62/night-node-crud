var express = require('express')
var cors = require('cors')
const mysql = require('mysql2');

const connection = mysql.createConnection({
  host: '157.245.59.56',
  port: '3366',
  user: '6202278',
  password: '6202278',
  database: '6202278'
});

var app = express()
app.use(cors())
app.use(express.json())

app.get('/callofduty', function (req, res, next) {
    connection.query(
        'SELECT * FROM `callofduty`',
        function(err, results, fields) {
         res.json(results);
        }
      );
})

app.get('/callofduty/:id', function (req, res, next) {
    const id = req.params.id;
    connection.query(
        'SELECT * FROM `callofduty` WHERE `id` = ?',
        [id],
        function(err, results) {
            res.json(results)
        }
      );
})

app.post('/callofduty', function (req, res, next) {
    connection.query(
        'INSERT INTO `callofduty`(`name`, `year`, `cost`, `avatar`) VALUES (?, ?, ?, ?)',
        [req.body.name, req.body.year, req.body.cost, req.body.avatar],
        function(err, results) {
            res.json(results)
        }
      );
})

app.put('/callofduty', function (req, res, next) {
    connection.query(
        'UPDATE `callofduty` SET `name`=?, `year`=?, `cost`=?, `avatar`=? WHERE id = ?',
        [req.body.name, req.body.year, req.body.cost, req.body.avatar, req.body.id],
        function(err, results) {
            res.json(results)
        }
      );
})

app.delete('/callofduty', function (req, res, next) {
    connection.query(
        'DELETE FROM `callofduty` WHERE id = ?',
        [req.body.id],
        function(err, results) {
            res.json(results)
        }
      );
})

app.listen(5000, function () {
  console.log('CORS-enabled web server listening on port 5000')
})